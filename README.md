# How to #

This project contains :
- a `Rapport_PRFO.pdf` file
- a `graphs` folder with various graphs in it
- a `src` folder with sources files in it
- a `ipf_profet_2022.html` file that contains the assignelent (in french)
- a `doc` folder with an ocamldoc generated documentation
- this very file

# Compilation #
use `make` command in bash to compile.
Binaries should appear in a `bin` folder.

There is also a `make clear` and `make doc` instruction.

# Execution #
The resulting binaries are `phase1`, `phase2`, `phase3` and `tests` (the later being
unfortunately not in a finished state. But there's still some printf-esque test commented and
uncommented in there...)

each programe `phase` refers to a specific phase in the assignement. To use them, you must
provide to the binary a path to a corresponding graph files (some already availabe examples are already
in the `graphs` folder)

Try these for examples :
`bin/phase1 graphs/phase1_gt2`

`bin/phase2 graphs/phase2_gt0`

`bin/phase3 graphs/phase3_gt1`