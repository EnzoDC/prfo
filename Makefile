# CREDIT TO https://v2.ocaml.org/manual/depend.html
# And Xarus (who i salvaged his beautiful makefile structure from ips)

OCAMLC=ocamlc
INCLUDES=-I ./obj# all relevant -I options here
OCAMLFLAGS= $(INCLUDES)# -g  # add other options for ocamlc here
# sources
SRCDIR=./src
SRCSml=$(notdir $(wildcard $(SRCDIR)/*.ml)) 
SRCSmli=$(notdir $(wildcard $(SRCDIR)/*.mli)) 
SRCS=$(SRCSml) $(SRCSmli)

# target
TARGETDIR=bin

# obj
OBJDIR=./obj
OBJS=$(addprefix $(OBJDIR)/, $(SRCSmli:mli=cmi)) $(addprefix $(OBJDIR)/, $(SRCSml:ml=cmo))
OBJScmo=$(addprefix $(OBJDIR)/, $(SRCSml:ml=cmo))
OBJScmi=$(addprefix $(OBJDIR)/, $(SRCSmli:mli=cmi))

# doc
DOCDIR=doc
DOCSTYLE=../style.css

.PHONY: all
all:
	$(MAKE) mkdirectory
	$(MAKE)	build
	$(MAKE) doc

.PHONY: hard
hard:
	$(MAKE) clean
	$(MAKE) mkdirectory
	$(MAKE) build

.PHONY: build
build: phase1 phase2 phase3 tests
	echo building...

#tests dependencies
testsOBJS= \
$(OBJDIR)/graph.cmo $(OBJDIR)/prioqueue.cmo \
$(OBJDIR)/dijkstra.cmo  $(OBJDIR)/solver.cmo $(OBJDIR)/analyse.cmo  $(OBJDIR)/tests.cmo

.PHONY: tests
tests: $(testsOBJS)
	$(OCAMLC) -o $(TARGETDIR)/$@ $(OCAMLFLAGS) $(testsOBJS)


# phase 1 dependencies
phase1OBJS= \
$(OBJDIR)/graph.cmo $(OBJDIR)/prioqueue.cmo $(OBJDIR)/dijkstra.cmo \
$(OBJDIR)/analyse.cmo  $(OBJDIR)/phase1.cmo

.PHONY: phase1
phase1: $(phase1OBJS)
	$(OCAMLC) -o $(TARGETDIR)/$@ $(OCAMLFLAGS) $(phase1OBJS)


# phase 2 dependencies
phase2OBJS= \
$(OBJDIR)/analyse.cmo $(OBJDIR)/graph.cmo $(OBJDIR)/prioqueue.cmo \
$(OBJDIR)/solver.cmo $(OBJDIR)/phase2.cmo

.PHONY: phase2
phase2: $(phase2OBJS)
	$(OCAMLC) -o $(TARGETDIR)/$@ $(OCAMLFLAGS) $(phase2OBJS)

# phase 3 dependencies
phase3OBJS=  \
$(OBJDIR)/analyse.cmo $(OBJDIR)/graph.cmo $(OBJDIR)/prioqueue.cmo \
$(OBJDIR)/dijkstra.cmo $(OBJDIR)/solver.cmo $(OBJDIR)/phase3.cmo

.PHONY: phase3
phase3: $(phase3OBJS)
	$(OCAMLC) -o $(TARGETDIR)/$@ $(OCAMLFLAGS) $(phase3OBJS)


#dependencies (theses rarely happens, but they do (make doc for instance)...)
$(OBJDIR)/%.cmi: $(SRCDIR)/%.mli
	$(OCAMLC) -o $@ $(OCAMLFLAGS) -c $<
$(OBJDIR)/%.cmo: $(SRCDIR)/%.ml
	$(OCAMLC) -o $@ $(OCAMLFLAGS) -c $<

.PHONY: doc
doc: $(OBJScmi)
	mkdir -p $(DOCDIR)
	ocamldoc -d $(DOCDIR) $(INCLUDES) -html $(SRCDIR)/*.ml  $(SRCDIR)/*.mli -css-style $(DOCSTYLE)

.PHONY: mkdirectory
mkdirectory:
	mkdir -p $(TARGETDIR)
	mkdir -p $(OBJDIR)

.PHONY: clean
clean:
	rm -rf $(TARGETDIR)
	rm -rf $(OBJDIR)
	rm -rf $(DOCDIR)
	rm -rf $(SRCDIR)/*.mk

# copy and pasted + tweaked from a ocamldep
./obj/analyse.cmo :      ./obj/analyse.cmi      ./src/analyse.ml
./obj/analyse.cmx analyse.o :      ./obj/analyse.cmi      ./src/analyse.ml
./obj/analyse.cmi :
./obj/dijkstra.cmo :      ./obj/prioqueue.cmi      ./obj/graph.cmi      ./obj/dijkstra.cmi      ./src/dijkstra.ml
./obj/dijkstra.cmx dijkstra.o :      ./obj/prioqueue.cmi      ./obj/prioqueue.cmx      ./obj/graph.cmi      ./obj/graph.cmx      ./obj/dijkstra.cmi      ./src/dijkstra.ml
./obj/dijkstra.cmi :      ./obj/graph.cmi
./obj/graph.cmo :      ./obj/graph.cmi      ./src/graph.ml
./obj/graph.cmx graph.o :      ./obj/graph.cmi      ./src/graph.ml
./obj/graph.cmi :
./obj/phase1.cmo phase1.cmi :      ./obj/graph.cmi      ./obj/dijkstra.cmi      ./obj/analyse.cmi      ./src/phase1.ml
./obj/phase1.cmx phase1.o phase1.cmi :      ./obj/graph.cmi      ./obj/graph.cmx      ./obj/dijkstra.cmi      ./obj/dijkstra.cmx      ./obj/analyse.cmi      ./obj/analyse.cmx      ./src/phase1.ml
./obj/phase2.cmo phase2.cmi :      ./obj/solver.cmi      ./obj/graph.cmi      ./obj/analyse.cmi      ./src/phase2.ml
./obj/phase2.cmx phase2.o phase2.cmi :      ./obj/solver.cmi      ./obj/solver.cmx      ./obj/graph.cmi      ./obj/graph.cmx      ./obj/analyse.cmi      ./obj/analyse.cmx      ./src/phase2.ml
./obj/phase3.cmo phase3.cmi :      ./obj/solver.cmi      ./obj/graph.cmi      ./obj/dijkstra.cmi      ./obj/analyse.cmi      ./src/phase3.ml
./obj/phase3.cmx phase3.o phase3.cmi :      ./obj/solver.cmi      ./obj/solver.cmx      ./obj/graph.cmi      ./obj/graph.cmx      ./obj/dijkstra.cmi      ./obj/dijkstra.cmx      ./obj/analyse.cmi      ./obj/analyse.cmx      ./src/phase3.ml
./obj/prioqueue.cmo :      ./obj/prioqueue.cmi      ./src/prioqueue.ml
./obj/prioqueue.cmx prioqueue.o :      ./obj/prioqueue.cmi      ./src/prioqueue.ml
./obj/prioqueue.cmi :
./obj/solver.cmo :      ./obj/prioqueue.cmi      ./obj/graph.cmi      ./obj/solver.cmi      ./src/solver.ml
./obj/solver.cmx solver.o :      ./obj/prioqueue.cmi      ./obj/prioqueue.cmx      ./obj/graph.cmi      ./obj/graph.cmx      ./obj/solver.cmi      ./src/solver.ml
./obj/solver.cmi :      ./obj/graph.cmi
./obj/tests.cmo tests.cmi :      ./obj/solver.cmi      ./obj/graph.cmi      ./obj/dijkstra.cmi      ./obj/analyse.cmi      ./src/tests.ml
./obj/tests.cmx tests.o tests.cmi :      ./obj/solver.cmi      ./obj/solver.cmx      ./obj/graph.cmi      ./obj/graph.cmx      ./obj/dijkstra.cmi      ./obj/dijkstra.cmx      ./obj/analyse.cmi      ./obj/analyse.cmx      ./src/tests.ml
