(**This program answers the second phase of the project assignement
  it calls [phase1 Sys.argv.(1)]*)

(*importing required modules*)
module G = Graph.WeightedStringGraph
module S = Solver.Phase2SolverImplem(G)

(**
    This function takes a path to a file representing a {! Graph.WeightedGraph}, and paths to schedule, 
    (as described in the project requirements) and finds a (hopefully the best) scheduling resolving any conflicts
    using the algorithm implemented in {! Solver}

    {b parameters} 
        - string representing a file path to a graph file
    
    @return a unit and prints in stdout a string representing the found scheduling satisfying the given graph in form of list of path found with format :
      {[<starting node> - <first time of departure> -> <second node> - <second time of departure> -> ... <destination node>
        <starting node> - <first time of departure> -> <second node> - <second time of departure> -> ... <destination node>
        ...
        <starting node> - <first time of departure> -> <second node> - <second time of departure> -> ... <destination node>
        total time : <total time> 
      ]}
    @raise Sys_error msg if the given file was not found
    @raise Scanf.Scan_failure msg if the graph file was not in a good format
    @raise Failure "Unable to resolve (are all paths possible..?)" if the solver was not able to found solutions 
    @see <../src/solver.ml> implementation of algorithm solving this scheduling probleme {! Solver}
    @see <../ipf_projet_2022.html> the project assignement
*)
let phase2 path_to_graph_file =
  (*Gets the returning value from file*)
  let (graph_li, paths_li) = Analyse.analyse_file_2 path_to_graph_file in
  (*get solved path and length using Solver algorithm*)
    try 
      (*Build graph from its representation given by Analyse.analyse_file_1*)
      let graph = G.build_graph_from_analyse graph_li in
      (*Buid appropriate lists*)
      let tuplelists = List.map S.path_to_tuplelist paths_li in
      let sol_unformated = S.Timetable.find_best_time_table (S.Timetable.init_tt tuplelists graph) graph 10000 in
      let (time, table) = sol_unformated in
      (*print result*)
      Analyse.output_sol_2 (S.tt_to_sol_format sol_unformated) ; Format.printf "%d" time
    with
       _ -> let err = Format.sprintf "Unable to resolve (are all paths possible..?)" in failwith err

let _ = phase2 Sys.argv.(1)