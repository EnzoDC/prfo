(**
Solver of phase 2 of the homework
    *)

module type Phase2Solver =
    sig 

    (**Module of type WeightedGraph
    @see '../src/graph.mli' \'s signature of {! Graph.WeightedGraph}
    *)
    module WG : Graph.WeightedGraph

    (**Simple alias*)
    type node = WG.node

    (**Simple alias*)
    type graph = WG.graph

    (**
    Module IntMap for the Timetable module
    @see <https://v2.ocaml.org/api/Map.Make.html> \ 
    *)
    module IntMap : Map.S with type key = int

    (**
    Module to represent a scheduling inside a graph.
    for exemple, for a list of paths :
    {[
    c --> b --> e
    a --> b --> c --> d
    ]}
    in
    {[
      4
      a b 1
      b c 3
      e b 2
      c d 4
    ]}
    we get the timetable :
    {[
    |0 -> |0 -> c-b
    |     |3 -> b-e
    |
    |1 -> |0 -> a-b
    |     |1 -> b-c
    |     |4 -> c-d

    total time : 8
    ]}
    which is internally represented with type t [(int * ((WG.node*WG.node) IntMap.t) IntMap.t)]
    *)
    module Timetable :
      sig

        (**
        Type of one time table
        *)
        type t = (int * ((WG.node*WG.node) IntMap.t) IntMap.t)

        (**
        [init_tt edge_list graph] return the corresponding initial timetable of multiple paths in [graph]

        {b parameters} 
            - [edge_list] : a list of list of edges representing paths to schedule (see path to tuple_list)
            - [graph]     : the graph in which we do the scheduling

        @return timetable with initial naive scheduling
        @raise Not_found if the paths aren't possible in [graph]
        *)
        val init_tt : ((WG.node * WG.node) list) list -> WG.graph -> t

        (**
        prints a timetable on stdout

        {b parameters} 
            - [printf_node] : a callback function to print nodes
            - [timetable]   : timetable to print

        @return unit ()
        @raise Nothing \
        *)
        val printf : (WG.node -> unit) -> t -> unit

        (**
        [resolve_conflict tt conflict graph] solves the earliest conflict found in [tt]

        {b parameters} 
            - [tt] : timetable where to solve conflict
            - [conflict] : tuple of form [(col1, col2, time1, time2, edge)] indicating where the conflict is in [tt]
            - [graph] : graph in which the conflict happened
        @return tuple of form ([tt1], [tt2]) with the two possible resolution of the conflict 
        @raise Not_found if [edge] does not exists in graph 
        *)
        val resolve_conflict : t -> (int * int * int * int * (WG.node * WG.node)) -> graph -> (t*t)

        (**
        [find_best_time_table tt graph maxdepth] iterates with a {i search a bound} algorithm to find the best timetable without any
        conflicts. if the number of recursive calls is greater than [maxdepth], we return instead the best timetable found within
        this limit. by default, it is the solution found with a greedy algorithm (instead of using module {! Prioqueue}) 

        {b parameters} 
            - [tt] : timetable to solve
            - [graph] : graph in which the conflict happened
            - [maxdepth] : maximum recursion depth
        @return tt without any conflict
        @raise Nothing \
        *)
        val find_best_time_table : t -> graph -> int -> t

      end

    (**
    [path_to_tuplelist node_list] returns the same path but with edges instead.
    Example :
    {[
    ["a","b","c","d","e"]
    -> [("a","b"),("b","c"),("c","d"),("d","e")]
    ]}

    {b parameters} 
        - [node_list] : list of nodes
    @return list of edges
    @raise Nothing \
    *)
    val path_to_tuplelist : WG.node list -> (WG.node * WG.node) list

    (**
    [tt_to_sol_format tt] returns [tt] in the format required for [Analyse.output_sol_2]

      {b parameters} 
          - [tt] : timetable to convert
      @return list with of tuples of (path, duration) 
      @raise Nothing \
      *)
    val tt_to_sol_format : Timetable.t -> (node list * int list) list
    end

(**
Functor to un-abstract our module 
  *)
module Phase2SolverImplem (WeightedGraph: Graph.WeightedGraph): Phase2Solver with module WG = WeightedGraph