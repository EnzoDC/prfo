module type Phase2Solver =
  sig
    module WG : Graph.WeightedGraph
    module IntMap : Map.S with type key = int
    type node = WG.node
    type graph = WG.graph
    module Timetable :
      sig
        type t = (int * ((WG.node*WG.node) IntMap.t) IntMap.t)
        val init_tt : ((WG.node * WG.node) list) list -> WG.graph -> t
        val printf : (WG.node -> unit) -> t -> unit
        val resolve_conflict : t -> (int * int * int * int * (WG.node * WG.node)) -> graph -> (t*t)
        val find_best_time_table : t -> graph -> int -> t
      end
    val path_to_tuplelist : WG.node list -> (WG.node * WG.node) list
    val tt_to_sol_format : Timetable.t -> (node list * int list) list
  end

module Phase2SolverImplem (WeightedGraphImplem : Graph.WeightedGraph) =
struct
  module WG = WeightedGraphImplem
  type node = WG.node
  type graph = WG.graph
  module NodeMap = WG.NodeMap
  module IntMap = Map.Make(Int)
  (*type path = node list*)

  let find_first_opt_predicate fbool map =
    IntMap.fold (fun k v acc -> 
      if ((fbool k v)&&(acc = Option.none)) then 
        Some (k,v)
      else 
        acc
    ) map Option.none

  module Timetable =
    struct
      type t = (int * ((node*node) IntMap.t) IntMap.t)
      
      let compare_edge (n1,n2) (n3,n4) =
       (((n1=n3)&&(n2=n4))||((n1=n4)&&(n2=n3)))

      let equality tt1 tt2 =
        let ((time1, table1),(time2,table2)) = (tt1,tt2) in
        (time1=time2)&&(IntMap.equal (fun x y -> IntMap.equal (fun x y -> compare_edge x y) x y) table1 table2)
      
      let compare_edge_xor (n1,n2) (n3, n4) =
        if (n1=n2)||(n3=n4) then
          false 
        else
          (((n1=n3)&&(n2=n4))||((n1=n4)&&(n2=n3)))
      
      let init_tt paths_tuple_list graph =
        let aux_build_one_row path_tuple graph = (*we first only build one row out of a (n1,n2) list of our timetable*)
          List.fold_left (
            fun (t,map) (n1,n2) -> (
              let new_t = (WG.edge_value n1 n2 graph)+t in
                (new_t, IntMap.add t (n1,n2) map)
              )
            ) (0, IntMap.empty) path_tuple
        in
        let (time, table, index) =
          List.fold_left (  (*this second Fold makes the whole time-table out of our list of (list of tuples)*)
            fun (total_time,map_of_map,index) path_tuple -> (
              let (t,map) = (aux_build_one_row path_tuple graph) in
              let new_total_time = (max t total_time) in
              (new_total_time, IntMap.add index map map_of_map, index+1)
              )
            ) (0, IntMap.empty, 0) paths_tuple_list
        in
        (time, table)

      (*pretty-prints a tt*)
      let printf printnodetuple tt =
        let (time, table) = tt in
        Format.printf "total time = %d\n" time ;
        IntMap.iter (
          fun key vmap -> (
            Format.printf "|%d --> \n" key ;
            (IntMap.iter (
              fun k_ (n1, n2) -> Format.printf "|\t\t|%d --> " k_ ; printnodetuple n1 ; 
              Format.printf "-" ; printnodetuple n2 ; Format.printf "\n"
              ) vmap
            )
          )
        ) table ;
        Format.printf "\n"

      (*pretty-prints a tt on stderr*)
      let eprintf eprintnodetuple tt =
        let (time, table) = tt in
        Format.eprintf "total time = %d\n" time ;
        IntMap.iter (
          fun key vmap -> (
            Format.eprintf "|%d --> \n" key ;
            (IntMap.iter (
              fun k_ (n1, n2) -> Format.eprintf "|\t\t|%d --> " k_ ; eprintnodetuple n1 ; 
              Format.eprintf "-" ; eprintnodetuple n2 ; Format.eprintf "\n"
              ) vmap
            )
          )
        ) table ;
        Format.printf "\n"
      
      (*we produce a timetable on durations where only a given could happen*)
      let select_timewindows time duration row =
        let (l, x, r) = IntMap.split (time+duration) row in
        if (time <> 0) then
          let (ll,xx,rr) = IntMap.split (time-1) l in
            rr
        else 
            l
      
      (*Look for conflicts for the value tt.[key].[time] = (n1,n2)
        anywhere else in the tt*)
      let search_conflict key time duration (n1,n2) table =
        (*let (time_, table) = tt in*)
        (*let _ = Format.printf "we're looking for collision with (%d,%d) up to t%d\n" key time duration in*)
        let table_to_search = IntMap.remove key table in (*we remove the current row (to avoid checking against itself)*)
        let table_adjusted_timeframe = 
          IntMap.map (select_timewindows time duration) table_to_search
          (*we limit every row on the duration where a possible collision could happen*)
        in
        (*let _ = IntMap.iter (fun k v ->  (IntMap.iter (fun kt vt -> Format.printf "k%d t%d\n" k kt) 
          v)) table_adjusted_timeframe in*) 
        let found =
          find_first_opt_predicate (
            fun k v -> (IntMap.exists (fun x v -> (
                (*let _ = Format.printf "k%d kt%d\n" k x in*)
                compare_edge_xor v (n1,n2))) v)
            ) table_adjusted_timeframe
        in
        match found with
          | None -> None
          | Some (key, value) ->
            let (keytime,n) = IntMap.find_first (fun k ->
              let (x,y) = (IntMap.find k value) in
              (compare_edge_xor (x,y) (n1,n2))) value in
            Some (key, keytime, (n1,n2))
        (*we return a (index, keytime, row) option ; useful for checking*)
      
      (*return Some (index, keytime, edge) for the smallest found timestamp, else None.*)
      let find_smallest_timestamp_in_tt table =
        let aux index row value = 
          (*checks value (type option) against all value of row. takes the smallest, 
            or existing value if value is None*)
          let row_without_stationary_nodes =
            IntMap.filter_map (fun time (n1,n2) -> if (n1=n2) then Option.none else Some (n1,n2)) row
          in
          match value with
            | None -> (
              let value_found = IntMap.find_first_opt (fun x -> (x >= 0)) row_without_stationary_nodes in
                match value_found with
                | None -> None
                | Some (newkeytime, newedge) -> Some (index, newkeytime, newedge)
            )
            | Some (i, keytime, edge) ->
              (
              let value_found = 
                IntMap.find_first_opt (fun k -> (k<=keytime)) row_without_stationary_nodes
              in
              match value_found with
                | None -> Some (i, keytime, edge)
                | Some (newkeytime, newedge) -> Some (index, newkeytime, newedge)
            )
        in
      IntMap.fold aux table Option.none
      
      let look_for_conflict tt graph =
        let (time, table) = tt in
        let rec aux_find_earliest_conflict acctable conflict =
          let value = find_smallest_timestamp_in_tt acctable in
          (*value is a (index,time,edge) option
            indicating the earliest move 
            done in the timetable*)
            match value with
              |None -> conflict (*no conflicts in our tt*)
              |Some (index, keytime, edge) ->
              (*we remove the searched timestamp from the acctable*)
                let row = IntMap.find index acctable in
                let new_row = IntMap.remove keytime row in
                let new_table = (IntMap.add index new_row acctable) in
              (*we find a conflict in tt for the found timestamp*)
                let duration = let (n1,n2) = edge in WG.edge_value n1 n2 graph in
                let found_conflict = search_conflict index keytime duration edge table in
                if (conflict = Option.none) then (
                  if (found_conflict=Option.none) then
                    aux_find_earliest_conflict new_table Option.none
                  else
                    let (index2, keytime2, edge2) = Option.get found_conflict in
                    (*we put the found conflict since we don't have any conflicts yet*)
                    aux_find_earliest_conflict new_table (Some (index, index2, keytime, keytime2, edge))
                )
                (*we have to compare the two conflicts*)
                else(
                  let (index_acc, index2_acc, keytime_acc, keytime2_acc, edge_acc) =
                    Option.get conflict in
                  let time_of_conflict_acc = (max keytime2_acc keytime2_acc) in
                    (*if we already have a conflict earlier than the earliest timestamp remaning in acctt
                      stop searching for a conflict*)
                    if (keytime > time_of_conflict_acc) then conflict
                    else (
                      (*no foudn conflict -> we keep the actual conflict*)
                      if (found_conflict = Option.none) then
                        aux_find_earliest_conflict new_table conflict
                      else(
                        (*we compare the date of the conflict, and keep the earliest one*)
                        let (index2, keytime2, edge2) = Option.get found_conflict in
                        let time_of_conflict = (max keytime keytime2) in
                          if (time_of_conflict_acc < time_of_conflict) then
                            aux_find_earliest_conflict new_table conflict
                          else
                            aux_find_earliest_conflict new_table (Some (index, index2, keytime, keytime2, edge))
                      )
                    )
                )
        in
        aux_find_earliest_conflict table Option.none

      let resolve_conflict tt (index, index2, keytime, keytime2, (n1, n2)) graph =
        let duration = WG.edge_value n1 n2 graph in

        let (time, table) = tt in
        let offset_keys keytime time_to_wait row =
          let (s,t) = IntMap.find keytime row in
          let updated_row = IntMap.fold
            (fun key edge mapacc ->
              if (key < keytime) then
                IntMap.add key edge mapacc
              else
                IntMap.add (key+time_to_wait) edge mapacc
            ) row (IntMap.empty)
          in
          IntMap.add keytime (s,s) updated_row
        in
        (*solved tables*)
        let time_to_wait_1 = (keytime2 + duration) - keytime in
        let time_to_wait_2 = (keytime + duration) - keytime2 in
        let solution1 = IntMap.add index (offset_keys keytime time_to_wait_1 (IntMap.find index table)) table in
        let solution2 = IntMap.add index2 (offset_keys keytime2 time_to_wait_2 (IntMap.find index2 table)) table in
        
        (*getting the total duration of theses timetables*)
        let maxkeytime1 = IntMap.fold (
          fun k v acc ->
            let (maxkey,(n1,n2)) = (IntMap.max_binding v) in
            (max acc (maxkey+(WG.edge_value n1 n2 graph)))
          ) solution1 time
        in
        let maxkeytime2 = IntMap.fold (
          fun k v acc -> 
            let (maxkey,(n1,n2)) = (IntMap.max_binding v) in
            (max acc (maxkey+(WG.edge_value n1 n2 graph)))
          ) solution2 time
        in
        ((maxkeytime1, solution1), (maxkeytime2,solution2)) 
        
      (*we find one solution, this is a upper bound for our search*)
      let find_one_time_table tt graph =
        let rec aux tt  =
          let conflicts = look_for_conflict tt graph in
            if (conflicts = Option.none) then
            (*we found a solution !*)
              tt
            else (
              let ((time1, table1),(time2, table2)) =
                resolve_conflict tt (Option.get conflicts) graph in
              if (time1 > time2) then
                aux (time2, table2) (*we iterate one the solution with the shortest path yet*)
              else
                aux (time1, table1)
            )
        in
        aux tt
      
      (*solve conflicts and find the best solution*)
      (*every time we resolve a conflict, we put the two solution of our time tables
      in a priority queue, sorting by the global time of the tt (longest path present in the timetable)
      for a given tt, its actual global time is actually a lower bound of any subsequent solutions that will appear out of this tt
      by using a priority queue, and dismissing tt that have longer time than any solution found,
      we optimize our search and number of operations

      update : since i did not suceed in making this function work in a reasonable time, or without crashes, i add
      a max_depth arg, that prevents too long recursions..
      Sincerly 
                                                                                      -Anzo
      *)
      let find_best_time_table tt graph max_depth =
        let rec aux tt pq (besttimeyet, bestsolyet) max_depth=
          if max_depth < 0 then (*max recursion dephts hit. Return a valid solution (but not necessarily the best)*)
            (besttimeyet, bestsolyet)
          else
          let conflicts = look_for_conflict tt graph in
            if (conflicts = Option.none) then (
            (*we found a solution !*)
              let (time, solution) = tt in
              let updated_optimal =( (*we look if this solution is better or not, and we update accordingly*)
                if (time < besttimeyet) then
                  (time, solution)
                else
                  (besttimeyet,bestsolyet)
              )
              in
                if 
                  (Prioqueue.is_empty pq) then updated_optimal (*we are done*)
                else (*we still have values to check*)
                  let (ttime, next_tt, pq_updated) = Prioqueue.extract pq in
                  aux next_tt pq_updated updated_optimal (max_depth-1)
            )
            else (
              let ((time1, table1),(time2, table2)) =
                resolve_conflict tt (Option.get conflicts) graph in
              let pq_updated1 =
                if (time1 > besttimeyet) then (*we do not add unresolved tt that have at least more time than a current solution*)
                  pq
                else(
                  if not (Prioqueue.found_in pq (time1,table1) equality) then
                    Prioqueue.insert pq (-time1) (time1, table1)
                  else
                    pq
                )
              in
              let pq_updated2 =
                if (time2 > besttimeyet) then (*same exact thing here*)
                  pq_updated1
                else(
                  if not (Prioqueue.found_in pq_updated1 (time2,table2) equality) then
                    Prioqueue.insert pq_updated1 (-time2) (time2, table2)
                  else
                    pq_updated1
                )
              in
                if (Prioqueue.is_empty pq_updated2) then
                  (besttimeyet,bestsolyet) (*Should NEVER happen, but does since this code is a mess.. urgh*)
                  (*this however makes the function a bit more robust..*)
                else
                let (ttime, next_tt, pq_updated) = Prioqueue.extract pq_updated2 in
                aux next_tt pq_updated (besttimeyet, bestsolyet) (max_depth-1)
            )
        in
        aux tt Prioqueue.empty (find_one_time_table tt graph) max_depth 
    end

  (*
     ["a","b","c","d","e"]
     -> [("a","b"),("b","c"),("c","d"),("d","e")]
  *)
  let path_to_tuplelist paths =
    let rec aux li acc =
      match li with
      |[_] -> List.rev acc
      |n1::(n2::tl) -> aux (n2::tl) ((n1,n2)::acc)
      |[] -> List.rev acc
    in
    aux paths []

  (*
     [("a","b"),("b","c"),("c","d"),("d","e")]
     -> ["e","d","c","b","a"]
  *)
  let tuplelist_to_path_rev pathtuplelist =
  (*let _ = List.iter (fun (x,y) -> Format.printf "%s-%s " x y) pathtuplelist; Format.printf "\n" in*)
    let rec aux li acc =
      match li with
      |(n1,n2)::[] -> (n2::(n1::acc))
      |(n1,n2)::q -> aux q (n1::acc)
      |[] -> acc (*should not happen*)
    in 
    let sol = aux pathtuplelist [] in
    (*let _ = List.iter (Format.printf "%s->") sol ; Format.printf "\n" in*)
    sol
  
  let aux_build_one_row pathtuple graph = (*we first only build one row out of a (n1,n2) list of our timetable*)
    List.fold_left (
      fun (t,map) (n1,n2) -> (
        let new_t = (WG.edge_value n1 n2 graph)+t in
          (new_t, IntMap.add t (n1,n2) map)
        )
      ) (0, IntMap.empty) pathtuple

  (*transforms a tt into a 
  (string list*int list) list*)
  let tt_to_sol_format tt =
    let (time, table) = tt in
    IntMap.fold (
      fun index row accli ->(
        let (tuplelist,timelist) =(
          IntMap.fold (
            fun keytime (n1,n2) (edgelist,timelist) ->
              if (n1 <> n2) then
                (((n2,n1)::edgelist),keytime::timelist)
              else
                (edgelist,timelist)
          ) row ([],[])
        )
        in ((tuplelist_to_path_rev tuplelist), List.rev (timelist)))::accli
    ) table []

  let path_time path_tuple_list graph = 
    List.fold_left (fun x (n1,n2) -> ((WG.edge_value n1 n2 graph)+x)) 0 path_tuple_list

end