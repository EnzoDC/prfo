(**Dijkstra algorithm on WeightedGraph*)

(**
Dijkstra module (working with every weighted graph of type WeightedGraph)
*)
module type DijkstraSolver =
sig
    (**Module of type WeightedGraph
    @see '../src/graph.mli' \'s signature of {! Graph.WeightedGraph}
    *)
    module WG : Graph.WeightedGraph

    (**
    [dijkstra g start target] returns the shortest path and its lenghts bewteen the nodes
    [start] and node [target] in the graph [g] using
    Dijkstra's shortest path algorithm with a priority queue (see {! Prioqueue})

    {b parameters}
        - [g] : graph to find the shortest path in
        - [start] : node beginning of found path
        - [target] : node end of found path
    
    @return [(path: WG.node list, length: int)] with [path] representing the shortest found path, and [length] its weight 
    (sum of all the edges forming the path in [g])
    @raise Failure if no path where found in [g] (ex : disconnected graph)
    *)
    val dijkstra : WG.graph -> WG.node -> WG.node -> (WG.node list * int)
end

(**
Functor to build a Dijkstra implementation on a given WeightedGraph module
{e (and not on a module type)}
*)
module DijkstraSolverImplem (WeightedGraph : Graph.WeightedGraph): DijkstraSolver with module WG = WeightedGraph 