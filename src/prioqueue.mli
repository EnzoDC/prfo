(**
    Priority Queue simple Implem
    Taken and tweaked for my needs from
    https://v2.ocaml.org/manual/moduleexamples.html
*)

(**
    Type used for sorting our element in the priority queue
*)
type priority = int

(**
    Type of priority queue
*)
type 'a queue = Empty | Node of priority * 'a * 'a queue * 'a queue

(**
returns the Empty element

@return Empty
@raise Nothing \
*)
val empty : 'a queue

(**
[is_empty q] returns true if q is Empty

{b parameters} 
    - [q] : priority queue

@return bool (q=Empty)
@raise Nothing \
*)
val is_empty : 'a queue -> bool

(**
[insert q prio elt] returns a priority queue with [elt] inserted with priority [prio]

{b parameters} 
    - [q] : priority queue
    - [prio] : priority of the inserted element [elt]
    - [elt] : element to insert

@return a new priority queue with [elt] inserted with priority [prio]
@raise Nothing \
*)
val insert : 'a queue -> int -> 'a -> 'a queue

(**
[extract q] extracts the highest priority element in [q]

{b parameters} 
    - [q] : priority queue to extract from

@return tuple ([prio], [elt], [q']), with [q'] being [q] without [elt] of [prio] in
@raise Queue_is_empty if [(q = Empty)]
*)
val extract : 'a queue -> (int * 'a * 'a queue)

(**
[found_in q elt] indicates if an element [elt] is in [q]

{b parameters} 
    - [q] : priority queue to look [elt] in
    - [elt] : element to look for
    - [cmp] : compare function to define equality

@return bool indicating if an element [elt'] such as (compare [elt] [elt']) returns true
@raise Nothing \
*)
val found_in : 'a queue -> 'a -> ('a -> 'a -> bool) -> bool