(**
   Simple WeightedGraph implementation
*)
module type WeightedGraph =
sig
    (**
    type of the node of the graph
    *)
    type node

    (**
    Module of the type of the Node.
    We need to be able to form set out of nodes, hence the type of the module.
    @see <https://v2.ocaml.org/api/Set.OrderedType.html>  \ 
    *) 
    module NodeType : Set.OrderedType with type t = node

    (**
    Module NodeMap for the graph
    @see <https://v2.ocaml.org/api/Map.Make.html> \ 
    *)
    module NodeMap : Map.S with type key = node

    (**
    our graph is of type [(int NodeMap.t) NodeMap.t]

    Nodes are represented by a dictionary that maps any node to another dictionary.
    For each node, this later dictonary represents the node's neighbors through its keys,
    and the weights of the edges through its bindings.

    An example : Consider this weighted [graph] of three nodes (a) (b) (c) :
    {[
            (a) ---------- (b)
            |      5       |
            |              |  10
            |              |
            ------------- (c)
                    1           
        
    is depicted by the following map :

        ---------------------------                  
        .         KEYS: node      .                  ---------------
        .                         .                  .  KEYS: node .
        .       ( a: node )   ---------------------> .  (b node)------> 5: int
        .                         .                  .  (c: node)------> 1: int
        .                         .                  ---------------
        .                         .                  ---------------
        .                         .                  .  KEYS: node .
        .       ( b: node )   ---------------------> .  (a: node)------> 5: int
        .                         .                  .  (c: node)------> 10: int
        .                         .                  ---------------
        .                         .                  --------------- 
        .                         .                  .  KEYS: node .
        .       ( c: node )   ---------------------> .  (b: node)------> 10: int
        .                         .                  .  (a: node)------> 1: int
        .                         .                  ---------------
        ---------------------------
    ]}

    We find (a)'s neighbors by looking through the keys of the map [m = (Map.find a graph)]. 
    To get the weight of the edge [(a)--(c)], we look at [Map.find c m]. Note that this weight is
    also found at [Map.find a (Map.find c graph)]

    While our weighted graphs could be directed by such structure. Since we only work on
    undirected graphs, every functions that modify the topology of a graph assures that 
    any changes on a binding are reciprocal.

    *)
    type graph = (int NodeMap.t) NodeMap.t

    (**
    The empty graph
    *)
    val empty : graph

    (**
    Tests if a graph is empty

    {b parameters}
        - [graph] : graph to test
    
    @return [true] if the graph is empty, [false] otherwise.
    @raise Nothing \ 
    *)
    val is_empty : graph -> bool
    
    (**
    [mem n g] returns [true] if [n] is in [g], [false] otherwise

    {b parameters}
        - [n] : node
        - [g] : graph where to search node
    
    @return [true] if [n] is in [g], [false] otherwise
    @raise Nothing \ 
    *)
    val mem : node -> graph -> bool

    (**
    [add_node n g] adds a new node [n] in [g] if [n] is not already present in [g]
    
    {b parameters} 
        - [n] : node to add to graph [g]
        - [g] : graph in which [n] is to be added
    
    @return new graph [g] in which [n] is binded to [NodeMap.empty] (no neighbors).
    However, if a binding for [n] already exists in [g], this function returns [g] unchanged.
    @raise Nothing \ 
    *)
    val add_node : node -> graph -> graph

    (**
    [add_edge n1 n2 w g] adds a new edge between n1 and n2 of weight [w] in graph [g].
    If an edge bewteen the two nodes already exists, it updates its weight to [w].
    If a [n1] or [n2] do not already exist in [g], [n1] or [n2] is also added in [g]
    
    {b parameters} 
        - [n1] : node representing the first vertice of the edge to add
        - [n2] : node representing the second vertice of the edge to add
        - [w] : int weight of the edge between n1 and n2. Must be positive.
        - [g] : graph where to add the edge
    
    @return new graph [g] in with a edge between a [n1] and a [n2] of weight [w]
    @raise Invalid_argument if [w < 0]
    *)
    val add_edge : node -> node -> int ->graph -> graph

    (**
    [remove_node n g] removes the nodes [n] in [g]. 
    If there [n] was not in [g] before calling this function, then [g] is left unchanged.
    
    {b parameters} 
        - [n] : node to remove
        - [g] : graph to remove the node from
    
    @return new graph [g] without node [n] 
    @raise Nothing \
    *)
    val remove_node : node -> graph -> graph

    (**
    [remove_edge n1 n2 g] removes the edge between [n1] and [n2] in [g]. 
    If there was no edges between [n1] [n2] in [g] before calling this function, then [g] is left unchanged.
    
    {b parameters} 
        - [n1] : node representing the first vertice of the edge to add
        - [n2] : node representing the second vertice of the edge to add
        - [g] : graph to remove the edge from
    
    @return new graph [g] without the edge between [n1] and [n2].
    @raise Not_found if [n1] or [n2] is not in [g]
    *)
    val remove_edge : node -> node -> graph -> graph 

    (**
    [edge_value n1 n2 g] return the weight of the edge between [n1] and [n2] found in g.
    
    {b parameters} 
        - [n1] : node representing the first vertice of the edge
        - [n2] : node representing the second vertice of the edge
        - [g] : graph where the find the edge in
    
    @return value of the edge between [n1] and [n2]
    @raise Not_found if [n1] or [n2] is not in [g]
    *)
    val edge_value : node -> node -> graph -> int

   (**
    [succs n g] return the binding of [n] in [g] (neighbors)
    
    {b parameters} 
        - [n] : node to get the binding of
        - [g] : graph where the find the node
    
    @return int Nodemap.t representing the neighbors of [n]
    @raise Not_found if [n] is not in [g]
    *)
    val succs : node -> graph -> int NodeMap.t

    (**
        [fold_node f g init] computes [(f nN dN ... (f n1 d1 init)...)], 
        where [n1 ... nN] are the nodes of all bindings in the graph [g] (in increasing order), 
        and [v1 ... vN] are the associated data of [n1 ... nN].
    
    {b parameters} 
        - [f] : (node -> int NodeMap.t -> 'a -> 'a ) function f to apply the fold
        - [g] : graph to apply fold
        - [init] : 'a init value for the fold
    
    @return [(f nN dN ... (f n1 d1 init)...)]
    @raise Nothing \
    *)
    val fold_node : (node -> int NodeMap.t -> 'a -> 'a) -> graph -> 'a -> 'a

    (**
    unused
    val fold_edge : (node -> int -> 'a -> 'a) -> int NodeMap.t -> 'a -> 'a
    *)

    (*utils*)
    (**
    Pretty prints a graph [g] on stdout
    
    {b parameters} 
        - [g] : graph to print
    
    @return unit [()]
    @raise Nothing unless the user doesn't implement this well
    *)
    val printf : graph -> unit

    (**
    This function is intended to build a weightedgraph after a text file has been processed

    {b parameters}
        - [input_list] : list resulted from the processing of the function {! Analyse.analyse_file_1}
    
    @return {! graph} corresponding weighted graph represented in the input. 
            returns the {! empty} graph when the input is empty
    @raise Exception TODO
    @see <../src/analyse.mli> implementation of [analyse_file_1]
    *)
    val build_graph_from_analyse : (string * string * int) list -> graph

end

(**
    WeightedGraph implementation using string as nodes
*)
module WeightedStringGraph : WeightedGraph with type node = string