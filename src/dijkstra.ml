module type DijkstraSolver =
sig
    (**Module of type WeightedGraph*)
    module WG : Graph.WeightedGraph
    val dijkstra : WG.graph -> WG.node -> WG.node -> (WG.node list * int)
end

module DijkstraSolverImplem (WeightedGraphImplem : Graph.WeightedGraph) =
struct
    module WG = WeightedGraphImplem
    type node = WG.node
    type graph = WG.graph
    module NodeMap = WG.NodeMap
    module NodeSet = Set.Make(WG.NodeType)

    (*function building a set of the neighbor of n1 in graph*)
    let neighbor_set n1 graph =
      let aux_ kn dn set = NodeSet.add kn set in
      let n1_bindings = WG.succs n1 graph in
      (NodeMap.fold aux_ n1_bindings NodeSet.empty)

    (**
    
      Dijkstra graph source target :

      dist[source] = 0
      PQueue.insert(source)

        While not(PQ.is_empty)
          node <- PQ.extract()

          if (node = target)
            dist[target]
          else
            visited(node) -> true
            foreach nnode in neighbors(node) 
              if not(visited(nnode))
                  dist[nnode] = dist[node] + edge_value
                  prev[nnode] = node
                  PQueue.insert(nnode)
      *)
    let dijkstra graph source target =
      (*map of distance, every node is a dist infinity at the start*)
      let dist_init = NodeMap.add source 0 (NodeMap.map (fun n -> max_int) graph) in
      (*Making the unvisited set, using Set module...*)
      let unvisited_init = NodeMap.fold (fun n v s -> NodeSet.add n s) graph NodeSet.empty in
      (*making the prev map with node Option.t type*)
      let prev_init = (NodeMap.map (fun n -> Option.None) graph) in
      (*init of a Prioqueue with source in it*)
      let pq_init = Prioqueue.insert (Prioqueue.empty) 0 source in

      (*building output from dist and prev*)
      let result (dist,prev) target =
        let rec aux_rev_path node path =
          if node=source then (node::path)
          else(
            let prev_node = (NodeMap.find node prev) in
            (*error handling*)
            if (Option.is_none prev_node) then failwith "Can't resolve given graph. It might be a disconnected graph.\n"
            else aux_rev_path (Option.get prev_node) (node::path)
          )
        in
        (aux_rev_path target [], NodeMap.find target dist)
      in

      (*returns correct dist and prev*)
      let rec aux_visit graph dist prev unvisited pq =
        if (Prioqueue.is_empty pq) then
          (dist,prev)
        else
          let (prio,node_to_visit,pq_updated1) = Prioqueue.extract pq in
          if (node_to_visit=target) then
            (dist,prev)
          else
            let unvisited_updated = NodeSet.remove node_to_visit unvisited in
            let neighbors_not_visited = NodeSet.inter unvisited_updated (neighbor_set node_to_visit graph) in
            let (dist_updated, prev_updated, pq_updated2) =
              NodeSet.fold (
                fun n (d,p,pq) ->
                  let new_d = NodeMap.add n ((WG.edge_value n node_to_visit graph)+(NodeMap.find node_to_visit d)) d in
                  (
                    (new_d),
                    (NodeMap.add n (Option.some node_to_visit) p),
                    (Prioqueue.insert pq (NodeMap.find n new_d) n)
                  )
              ) neighbors_not_visited (dist, prev, pq_updated1)
            in
            aux_visit graph dist_updated prev_updated unvisited_updated pq_updated2
      in
      
      result (aux_visit graph dist_init prev_init unvisited_init pq_init) target
end