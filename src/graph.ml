module type WeightedGraph =
sig
    type node
    module NodeType : Set.OrderedType with type t = node
    module NodeMap : Map.S with type key = node
    type graph = (int NodeMap.t) NodeMap.t

    val empty : graph
    val is_empty : graph -> bool
    val mem : node -> graph -> bool

    val add_node : node -> graph -> graph
    val add_edge : node -> node -> int ->graph -> graph
    val remove_node : node -> graph -> graph
    val remove_edge : node -> node -> graph -> graph 
    val edge_value : node -> node -> graph -> int

    val succs : node -> graph -> int NodeMap.t
    val fold_node : (node -> int NodeMap.t -> 'a -> 'a) -> graph -> 'a -> 'a
    (*val fold_edge : (node -> int -> 'a -> 'a) -> int NodeMap.t -> 'a -> 'a*)

    val printf : graph -> unit
    val build_graph_from_analyse : (string * string * int) list -> graph
end

module WeightedStringGraph : WeightedGraph with type node = string =
struct
    type node = string
    module NodeType = String
    module NodeMap = Map.Make(String)
    type graph = (int NodeMap.t) NodeMap.t 

    let empty = NodeMap.empty
    let is_empty g = NodeMap.is_empty g

    let mem = NodeMap.mem

    let add_node n g = 
      if not (mem n g) then
        NodeMap.add n NodeMap.empty g
      else
        g
    
    let remove_node n g = 
      (*removing the node n in g*)
      let g_ = NodeMap.remove n g in
      (*we then use NodeMap.map to remove every edges pointing to n*)
      let aux x = NodeMap.remove n x in
        NodeMap.map aux g_


    let add_edge n1 n2 w g =
      (*we only want positive weights w. we proof user input*)
      if w < 0 then
        invalid_arg "weight must be a positive integer."
      else
      try
        (* We first get the actual node n1 and n2*)
        let find_and_add_if_not_exist = 
          fun n g -> (if (mem n g) then NodeMap.find n g else NodeMap.find n (add_node n g))
        in
        let n1_old_value = find_and_add_if_not_exist n1 g in
        let n2_old_value = find_and_add_if_not_exist n2 g in
          (*we add the new edge and it weight to new nodes n1 and n2*)
          let n1_new_value = NodeMap.add n2 w n1_old_value in
          let n2_new_value = NodeMap.add n1 w n2_old_value in
            (*we update the graph with theses new nodes*)
            NodeMap.add n1 n1_new_value (NodeMap.add n2 n2_new_value g)
      with
        (*This should __not__ happen. If this error message appears, something is terribly wrong*)
        Not_found ->  let err = Format.sprintf "Error (add_edge %s %s) : Node not found.\n 
        This should __not__ happen. If this error message appears, something is terribly wrong" 
        n1 n2 in failwith err
    
    let remove_edge n1 n2 g =
      try
        (* We first get the actual node n1 and n2*)
        let n1_old_value = NodeMap.find n1 g in
        let n2_old_value = NodeMap.find n2 g in
          (*we add the new edge and it weight to new nodes n1 and n2*)
          let n1_new_value = NodeMap.remove n2 n1_old_value in
          let n2_new_value = NodeMap.remove n1 n2_old_value in
            (*we update the graph with theses new nodes*)
            NodeMap.add n1 n1_new_value (NodeMap.add n2 n2_new_value g)
      with
        Not_found -> let err = Format.sprintf "Error (remove_edge %s %s) : Node not found" n1 n2 in failwith err
    
    let edge_value n1 n2 g =
      if (n1=n2) then
        0
      else
        try
          let n1_binding = NodeMap.find n1 g in
          let value = NodeMap.find n2 n1_binding in
          value
        with
          Not_found -> let err = Format.sprintf "Error (edge_value %s %s) : Node not found" n1 n2 in failwith err

    let succs = 
      try
        NodeMap.find
      with
        Not_found -> let err = Format.sprintf "Error (succs) : Node not found" in failwith err
    
    let fold_node = NodeMap.fold
    let fold_edge = NodeMap.fold
    (*
       let fold_edge f g x = failwith "TODO"
    *)

    let printf g =
      let print_succ n w u =
        Format.printf "   %s : %d, \n" n w in
      let print_node n g_ u =
        Format.printf "%s : \n" n;
        NodeMap.fold print_succ g_ ();
        Format.printf "\n"
      in
      fold_node print_node g ()
    
    let build_graph_from_analyse list =
      let rec aux_builder graphacc li =
        match li with
        |[]-> graphacc
        |e::q -> let (node1, node2, weight) = e in
          aux_builder (add_edge node1 node2 weight graphacc) q
      in

      if (list=[]) then
        empty
      else
        aux_builder empty list

end