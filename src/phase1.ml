(**This program answers the first phase of the project assignement
  it calls [phase1 Sys.argv.(1)]*)

(*importing required modules*)
module G = Graph.WeightedStringGraph
module D = Dijkstra.DijkstraSolverImplem(G)

(**
    This function takes a path to a file representing a {! Graph.WeightedGraph}, a starting node and destination node, 
    (as described in the project requirements) and finds the shortest path between these two nodes using {! Dijkstra} algorithm

    {b parameters} 
        - string representing a file path to a graph file
    
    @return a string representing the shortest path found, with format :
      {[<time> : <starting node> <second node> ... <destination node>]}
      where time is the total weight of every edges in the path.
    @raise Sys_error msg if the given file was not found
    @raise Scanf.Scan_failure msg if the graph file was not in a good format
    @raise Node Not_found if no shortest path was found in a graph (ie : the given graph is disconnected)
    @see <../src/dijkstra.ml> implementation of {! Dijkstra}
    @see <../ipf_projet_2022.html> the project assignement
*)
let phase1 path_to_graph_file =
  (*Gets the returning value from file*)
  let (graph_li, (source, target)) = Analyse.analyse_file_1 path_to_graph_file in
  (*Build graph from its representation given by Analyse.analyse_file_1*)
  let graph = G.build_graph_from_analyse graph_li in
  (*get solved path and length using Dijkstra*)
  let (solved_path, length) = 
    try 
      D.dijkstra graph source target
    with
      Not_found -> let err = Format.sprintf "Unable to resolve shortest path in graph (probably disconnected graph)" in failwith err
  in
  (*print result*)
  Analyse.output_sol_1 length solved_path

let _ = phase1 Sys.argv.(1)