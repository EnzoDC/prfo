(**
  Test file to test various implem
*)

(*simple alias*)
module G = Graph.WeightedStringGraph
module D = Dijkstra.DijkstraSolverImplem(G)
module S = Solver.Phase2SolverImplem(G)

(*
Testing to build a graph from file
*)
(*simple Dijkstra test on a 3 node graph*)

(*
let _=
  let (li, path) = Analyse.analyse_file_1 "graphs/graph_test_1" in
  let graph = G.build_graph_from_analyse li in
  let (path, length) = D.dijkstra graph "a" "b" in
  G.printf graph ; 
  List.iter (Printf.printf "%s ") path ;
  Printf.printf "\n length = %d\n" length 

(*Simple Dijkstra test on a 6 node graph*)
let _ =
  let (li, path) = Analyse.analyse_file_1 "graphs/graph_test_2" in
  let graph = G.build_graph_from_analyse li in
  let (path, length) = D.dijkstra graph "a" "f" in
  G.printf graph ; 
  List.iter (Printf.printf "%s ") path ;
  Printf.printf "\nlength = %d\n" length 

(*Simple Dijkstra test on a 4 node graph, but the wanted path is impossible*)
let _ =
  let (li, path) = Analyse.analyse_file_1 "graphs/graph_test_3" in
  let graph = G.build_graph_from_analyse li in
  let (path, length) = D.dijkstra graph "a" "f" in
  G.printf graph ; 
  List.iter (Printf.printf "%s ") path ;
  Printf.printf "\nlength = %d\n" length
*)

(*
let _ =
  let int_tree = T.init_tree "a" in
  let int_tree = T.add_new_children int_tree ["a";"b";"c"] in
  let int_tree_2 = T.add_new_children (T.init_tree "b") ["a";"b";"c"] in
  let final_tree = T.add_tree int_tree int_tree_2 in
  let _ = T.printf (Format.print_string) final_tree in
  let final_tree_rem = T.remove_pos final_tree 2 in
  let _ = T.printf (Format.print_string) final_tree_rem in
  let final_tree_rem_aux = T.remove_child final_tree "b" in
  let _ = T.printf (Format.print_string) final_tree_rem_aux in
  ()
*)

let _ =
  let (li, paths) = Analyse.analyse_file_2 "graphs/phase2_gt4" in
  let graph = G.build_graph_from_analyse li in
  let path_tuple_list_list = List.map S.path_to_tuplelist paths in
  let tt = S.Timetable.init_tt path_tuple_list_list graph in
  (*let (a::q) = path_tuple_list_list in
  let (t,dict) = S.aux_build_one_row a graph in*)
  (*let _ = S.IntMap.iter (fun k (a,b) -> Format.printf "%d --> %s-%s\n" k a b) dict in
  let _ = List.iter (fun (a,b) -> Format.printf "%s-%s\n"a b) a in
  let _ = List.iter (fun a -> List.iter (Format.print_string) a) paths in*)
  let _ = S.Timetable.printf (Format.print_string) tt in
  (*let value = S.Timetable.look_for_conflict tt graph in
    match value with
    |Some (key1, key2, index1, index2, v) -> 
      let _ = Format.printf "conflict found : key : %d,%d time %d,%d\n" key1 key2 index1 index2 in
      let (tt1,tt2) = S.Timetable.resolve_conflict tt (key1, key2, index1, index2, v) graph in
      S.Timetable.printf (Format.print_string) tt1 ;
      S.Timetable.printf (Format.print_string) tt2 ;
      let value = S.Timetable.look_for_conflict tt1 graph in
      match value with
        |Some (key1, key2, index1, index2, v) ->
          let _ = Format.printf "conflict found : key : %d,%d time %d,%d\n" key1 key2 index1 index2 in
          let (tt3,tt4) = S.Timetable.resolve_conflict tt1 (key1, key2, index1, index2, v) graph in
          S.Timetable.printf (Format.print_string) tt3 ;
          S.Timetable.printf (Format.print_string) tt4 ;
        |None -> failwith "arhgsg"
    |None -> 
      failwith "argsh"*)
  let solved_tt = S.Timetable.find_best_time_table tt graph 10000 in
  let _ = S.Timetable.printf (Format.print_string) solved_tt in
  let output_good_format = S.tt_to_sol_format solved_tt in
  Analyse.output_sol_2 output_good_format
  